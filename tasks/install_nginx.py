from pathlib import Path
from pyinfra.operations import apt, files, server, systemd
from pyinfra import host
from pyinfra.api import deploy


@deploy("install nginx")
def install_nginx(config: dict):
    assert config["type"] == "nginx"
    deployment_name = config["name"]

    apt.packages("nginx")

    for server_config_file in config["configs"]:
        filename = Path(server_config_file).name
        files.template(
            server_config_file,
            f"/etc/nginx/sites-enabled/{filename}",
        )

    server.shell(
        [
            "nginx -t",
        ],
        name=f"ensure nginx files are correct",
    )

    systemd.service(
        name="start nginx",
        service="nginx.service",
        running=True,
        reloaded=True,
        enabled=True,
    )
