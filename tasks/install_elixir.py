from typing import Optional
from pyinfra.api import deploy
from pyinfra.operations import apt, files, server


@deploy("Install Elixir")
def install_elixir(
    elixir_version: Optional[str] = None,
    otp_version: Optional[str] = None,
):
    # elixir is non trivial to install because we can't
    # rely on the ubuntu package repo to be updated
    #
    # so we use the Erlang Solutions repository as recommended by elixir themselves.

    erlang_repo_deb_path = "/tmp/erlang-solutions.deb"
    files.download(
        name="download erlang solutions repo deb file",
        src="https://packages.erlang-solutions.com/erlang-solutions_2.0_all.deb",
        dest=erlang_repo_deb_path,
    )

    apt.deb(
        name="install erlang solutions repo",
        src=erlang_repo_deb_path,
    )

    # TODO: we don't need to update if we already installed the deb
    apt.update(cache_time=3600)

    # its in two separate steps as recommended by readme. who am i to judge
    apt.packages(
        name="install erlang",
        packages=[
            f"erlang={otp_version}" if otp_version else f"erlang",
            f"erlang-manpages={otp_version}" if otp_version else f"erlang-manpages",
        ],
    )
    apt.packages(
        name="install elixir",
        packages=[f"elixir={elixir_version}" if elixir_version else "elixir"],
    )

    server.shell(name="test elixir exists", commands=["elixir -v"])
