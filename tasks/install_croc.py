from packaging import version
from pyinfra.operations import apk, server
from pyinfra.facts.server import LinuxName
from pyinfra.api import deploy
from pyinfra import host

CROC_ALPINE_VERSION = version.parse("3.14")


@deploy("install croc")
def install_croc():
    if host.get_fact(LinuxName) == "Alpine":
        host_alpine_version = version.parse(host.data.alpine_version)

        if host_alpine_version >= CROC_ALPINE_VERSION:
            apk.packages(name="install croc via apk", packages=["croc"])
        else:
            # TODO download tar manually and install into prefix by myself
            server.shell(
                name="install croc via getcroc",
                commands=["curl https://getcroc.schollz.com | bash"],
            )
    else:
        assert False  # TODO support other distributions
