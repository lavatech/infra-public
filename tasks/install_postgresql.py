from pyinfra.api import deploy
from pyinfra.operations import apt, systemd, postgresql
from typing import Optional


@deploy("Install PostgreSQL")
def install_postgresql(config: Optional[dict]):
    assert config["type"] == "postgresql"
    version = config["version"]
    contrib_version = config["contrib_version"]

    apt.packages(
        name=f"Install psql packages (version {version}, contrib version {contrib_version})",
        packages=[
            f"postgresql-{version}",
            f"postgresql-client-{version}",
            f"postgresql-contrib={contrib_version}",
        ],
    )

    # assumes the package creates postgresql@12-main.service

    wanted_unit = f"postgresql@{version}-main.service"
    systemd.service(
        name="ensure systemd unit is up",
        service=wanted_unit,
        running=True,
        enabled=True,
        daemon_reload=True,
    )

    postgres_superuser_password = config.get("superuser_password")
    if postgres_superuser_password:
        postgresql.sql(
            f"ALTER USER postgres WITH PASSWORD '{postgres_superuser_password}'",
            su_user="postgres",
        )
