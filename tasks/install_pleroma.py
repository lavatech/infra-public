from pyinfra.operations import apt, server, files, systemd, postgresql
from pyinfra.api import deploy
from tasks.install_elixir import install_elixir

# TODO move repo() wrapper to util folder?
from tasks.install_elstat import repo


@deploy("install pleroma")
def install_pleroma(config: dict):
    db_config = config["db"]
    deployment_name = config["name"]
    runner_user = config["user"]
    assert db_config["local"]  # TODO support remote DB configurations
    install_elixir(
        elixir_version="1.13.0-1",
        otp_version=None,
    )

    apt.packages(
        name="install system depedencies",
        packages=[
            "sudo",
            "git",
            "build-essential",
            "cmake",
            "libmagic-dev",
            "imagemagick",
            "ffmpeg",
            "exiftool",
            "libimage-exiftool-perl",
            "erlang-dev",
            "erlang-parsetools",
        ],
    )

    files.directory(
        path="/opt/pleroma",
        present=True,
        mode=755,
        recursive=True,
    )

    server.group(
        group=runner_user,
    )

    remote_main_home_path = f"/opt/pleroma/{deployment_name}"
    remote_main_pleroma_path = f"/opt/pleroma/{deployment_name}/pleroma"

    server.user(
        user=runner_user,
        present=True,
        home=remote_main_home_path,
        shell="/bin/false",
        group=runner_user,
        ensure_home=True,
    )

    # commit pinning is done by having a separate branch on a mirror repo
    repo_output = repo(
        name="clone pleroma repo",
        src="https://gitlab.com/luna/pleroma.git",
        dest=remote_main_pleroma_path,
        branch="fl4pm",
        user=runner_user,
        group=runner_user,
    )

    local_config_path = config["config"]
    remote_config_path = f"{remote_main_pleroma_path}/config/prod.secret.exs"
    config_output = files.put(
        src=local_config_path,
        dest=remote_config_path,
        user=runner_user,
        group=runner_user,
        mode=500,
    )

    # download pleroma deps via mix
    server.shell(
        name="download pleroma deps",
        chdir=remote_main_pleroma_path,
        sudo_user=runner_user,
        commands=[
            "mix local.hex --if-missing --force",
            "mix local.rebar --if-missing --force",
            "mix deps.get",
        ],
        env={"MIX_ENV": "prod"},
    )

    # compile deps and compile pleroma
    server.shell(
        name="compile pleroma",
        chdir=remote_main_pleroma_path,
        sudo_user=runner_user,
        commands=["mix deps.compile", "mix compile"],
        env={"MIX_ENV": "prod"},
    )

    # map the following sql script into pyinfra

    # CREATE USER pleroma WITH ENCRYPTED PASSWORD 'aaa' CREATEDB;
    # CREATE DATABASE pleroma_dev;
    # ALTER DATABASE pleroma_dev OWNER TO pleroma;
    # \c pleroma_dev;
    # --Extensions made by ecto.migrate that need superuser access
    # CREATE EXTENSION IF NOT EXISTS citext;
    # CREATE EXTENSION IF NOT EXISTS pg_trgm;

    postgresql.role(
        role=db_config["user"],
        password=db_config["password"],
        superuser=True,
        login=True,
        sudo_user="postgres",
    )

    postgresql.database(
        database=db_config["name"],
        owner=db_config["user"],
        encoding="UTF8",
        sudo_user="postgres",
    )

    postgresql.sql(
        """
        CREATE EXTENSION IF NOT EXISTS citext;
        CREATE EXTENSION IF NOT EXISTS pg_trgm;
        """,
        database=db_config["name"],
        sudo_user="postgres",
    )

    server.shell(
        name="migrate database",
        chdir=remote_main_pleroma_path,
        sudo_user=runner_user,
        commands=["mix ecto.migrate"],
        env={"MIX_ENV": "prod"},
    )

    systemd_service_name = f"{config['service']}.service"
    files.template(
        src="./files/f.l4.pm/pleroma.service.j2",
        dest=f"/etc/systemd/system/{systemd_service_name}",
        env_dict={
            "deployment_name": deployment_name,
            "user": runner_user,
            "remote_main_home_path": remote_main_home_path,
            "remote_main_pleroma_path": remote_main_pleroma_path,
        },
    )

    systemd.service(
        systemd_service_name,
        running=True,
        restarted=repo_output.changed or config_output.changed,
        enabled=False,
        daemon_reload=True,
    )
