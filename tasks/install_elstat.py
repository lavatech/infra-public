from pyinfra.operations import apk, files, server, git, systemd, python
from pyinfra import host

from pyinfra.facts.files import Directory

from pyinfra.api import deploy, operation, FactBase
from pyinfra.operations.util.files import chown, unix_path_join

from .install_nodejs import install_nodejs


class CoolerGitBranch(FactBase):
    requires_command = "git"

    @staticmethod
    def command(repo):
        return "! test -d {0} || (cd {0} && git rev-parse --abbrev-ref HEAD)".format(
            repo
        )


class GitFetch(FactBase):
    def command(self, repo: str):
        return f"git -C {repo} fetch"

    def process(self, output):
        return output


class GitRevListComparison(FactBase):
    def command(self, repo: str, branch: str):
        return f"git -C {repo} rev-list HEAD..origin/{branch} | wc -l"

    def process(self, output):
        return output


class RawCommandOutput(FactBase):
    """
    Returns the raw output of a command.
    """

    def command(self, command):
        return command

    def process(self, output):
        return "\n".join(output)  # re-join and return the output lines


@operation(
    pipeline_facts={
        "git_branch": "target",
    }
)
def repo(
    src,
    dest,
    branch=None,
    pull=True,
    rebase=False,
    user=None,
    group=None,
    ssh_keyscan=False,
    update_submodules=False,
    recursive_submodules=False,
):
    """
    Clone/pull git repositories.

    + src: the git source URL
    + dest: directory to clone to
    + branch: branch to pull/checkout
    + pull: pull any changes for the branch
    + rebase: when pulling, use ``--rebase``
    + user: chown files to this user after
    + group: chown files to this group after
    + ssh_keyscan: keyscan the remote host if not in known_hosts before clone/pull
    + update_submodules: update any git submodules
    + recursive_submodules: update git submodules recursively

    Example:

    .. code:: python

        git.repo(
            name='Clone repo',
            src='https://github.com/Fizzadar/pyinfra.git',
            dest='/usr/local/src/pyinfra',
        )
    """

    # Ensure our target directory exists
    yield from files.directory(dest)

    if ssh_keyscan:
        raise NotImplementedError("TODO copypaste ssh_keyscan code")

    # Store git commands for directory prefix
    git_commands = []
    git_dir = unix_path_join(dest, ".git")
    is_repo = host.get_fact(Directory, path=git_dir)

    # Cloning new repo?
    if not is_repo:
        if branch:
            git_commands.append("clone {0} --branch {1} .".format(src, branch))
        else:
            git_commands.append("clone {0} .".format(src))

        host.create_fact(GitBranch, kwargs={"repo": dest}, data=branch)
        host.create_fact(CoolerGitBranch, kwargs={"repo": dest}, data=branch)
        host.create_fact(
            Directory,
            kwargs={"path": git_dir},
            data={"user": user, "group": group},
        )

    # Ensuring existing repo
    else:
        current_branch = host.get_fact(CoolerGitBranch, repo=dest)

        # always fetch upstream branches (that way we can compare if the latest
        # commit has changed, and then we don't need to execute anything!)
        host.get_fact(GitFetch, repo=dest)
        stdout = host.get_fact(GitRevListComparison, repo=dest, branch=branch)
        repository_has_updates = stdout[0] != "0"

        # since we immediately always fetch, we will always be modifying the
        # .git folder, and that folder MUST be owned by the correct user afterwards.
        if user or group:
            chown_command = chown(dest, user, group, recursive=True)
            host.get_fact(RawCommandOutput, command=chown_command.get_masked_value())

        if branch and current_branch != branch:
            git_commands.append("checkout {0}".format(branch))
            host.create_fact(GitBranch, kwargs={"repo": dest}, data=branch)
            host.create_fact(CoolerGitBranch, kwargs={"repo": dest}, data=branch)
            repository_has_updates = True

        if repository_has_updates and pull:
            if rebase:
                git_commands.append("pull --rebase")
            else:
                git_commands.append("pull")

    if update_submodules:
        if recursive_submodules:
            git_commands.append("submodule update --init --recursive")
        else:
            git_commands.append("submodule update --init")

    # Attach prefixes for directory
    command_prefix = "cd {0} && git".format(dest)
    git_commands = [
        "{0} {1}".format(command_prefix, command) for command in git_commands
    ]

    for cmd in git_commands:
        yield cmd

    # Apply any user or group if we did anything
    if git_commands and (user or group):
        yield chown(dest, user, group, recursive=True)


@deploy("install elstat")
def install_elstat(component_config: dict):
    assert component_config["type"] == "elstat"
    deployment_name = component_config["name"]
    assert deployment_name != "shenlong"  # lol

    # configure main user (holds source access)

    server.group("elstat")
    server.user("elstat", present=True, shell="/bin/sh", group="elstat")

    # configure runner user
    # (only runs the unit and has access to the binary and config.ini)
    runner_user = component_config["user"]
    server.user(
        runner_user,
        present=True,
        shell="/bin/sh",
        group=runner_user,
        groups=["elstat"],
    )

    local_config_path = component_config["config"]
    remote_config_path = f"/opt/elstat/{deployment_name}/config.ini"
    files.directory(
        f"/opt/elstat/{deployment_name}",
        mode=755,
        user=runner_user,
        group=runner_user,
    )
    config_output = files.put(
        local_config_path,
        remote_config_path,
        mode=700,
        user=runner_user,
        group=runner_user,
    )

    # download binary (enabling elstat group to access it)
    # TODO move this to configuration
    binary_output = files.download(
        name="Download the elstat binary",
        src="https://gitlab.com/elixire/elstat/-/jobs/1602359507/artifacts/raw/bin/elstat",
        dest="/opt/elstat/elstat",
        user="elstat",
        mode=755,
        sha256sum="a71e0953bdd5119529468ded05aa27f7a24e225f2727d278cb250d6a2260cafc",
    )

    # config files need to be accessed only by the respective runner users
    server.shell(
        [f"setfacl -m u:{runner_user}:r {remote_config_path}"],
        name="enable runner user's elstat config permissions",
    )

    systemd_unit = f"{component_config['service']}.service"
    files.template(
        src="./files/elstat/systemd.conf.j2",
        dest=f"/etc/systemd/system/{systemd_unit}",
        env_dict={
            "deployment_name": deployment_name,
            "port": component_config["port"],
            "user": component_config["user"],
        },
    )

    systemd.service(
        systemd_unit,
        running=True,
        restarted=binary_output.changed or config_output.changed,
        enabled=True,
        name=f"deploy elstat systemd unit ({systemd_unit})",
    )

    # install shenlong and create frontend files on
    # /opt/elstat/{deployment_name}/frontend_build

    remote_shenlong_path = "/opt/elstat/shenlong"
    repo_output = repo(
        name="clone shenlong (frontend) repository",
        src="https://gitlab.com/elixire/shenlong",
        dest=remote_shenlong_path,
    )

    if repo_output.changed:
        # ensure output directory exists
        # (only nginx needs to access it, so we're good leaving it as such)
        frontend_output_folder = f"/opt/elstat/{deployment_name}/frontend_build"
        files.directory(
            frontend_output_folder,
            user=runner_user,
            group=runner_user,
            mode=755,
        )

        server.shell(
            ["cd /opt/elstat/shenlong && npm ci"],
            name="install frontend deps",
        )

        server.shell(
            [
                "cd /opt/elstat/shenlong && npm run build",
                f"cp -vr /opt/elstat/shenlong/build/* {frontend_output_folder}",
            ],
            _env=component_config["frontend_config"],
            name=f"build frontend to {frontend_output_folder}",
        )

        # redo the directory perms but do it recursively as cp runs under root
        files.directory(
            frontend_output_folder,
            user=runner_user,
            group=runner_user,
            mode=755,
            recursive=True,
        )
