from pyinfra.operations import apk, files, server, git, systemd, pip
from pyinfra import host
from pyinfra.api import deploy

from .install_nodejs import install_nodejs


@deploy("install d1")
def install_d1(component_config: dict):
    assert component_config["type"] == "d1"
    deployment_name = component_config["name"]

    d1_user = component_config["user"]

    # configure main user (source & runner)
    server.group(
        d1_user,
    )
    server.user(
        d1_user,
        present=True,
        shell="/bin/sh",
        group=d1_user,
    )

    local_config_path = component_config["config"]
    remote_config_path = f"/opt/d1/{deployment_name}.ini"
    files.directory(
        f"/opt/d1",
        mode=555,
        user=d1_user,
        group=d1_user,
    )
    files.put(
        local_config_path,
        remote_config_path,
        mode=500,
        user=d1_user,
        group=d1_user,
    )

    files.template(
        src="./files/d1/run_forever.sh.j2",
        dest="/opt/d1/run_forever.sh",
        mode=500,
        user=d1_user,
        group=d1_user,
        env_dict={
            "remote_config_path": remote_config_path,
        },
    )

    repo_output = git.repo(
        name="clone d1 repository",
        src="https://gitlab.com/elixire/d1",
        dest="/opt/d1/d1",
        user=d1_user,
        group=d1_user,
    )

    pip.venv("/opt/d1/d1/venv", python="python3")
    pip.packages(
        packages=["/opt/d1/d1"],
        present=True,
        virtualenv="/opt/d1/d1/venv",
    )

    systemd_unit = f"{component_config['service']}.service"
    files.template(
        src="./files/d1/d1.service.j2",
        dest=f"/etc/systemd/system/{systemd_unit}",
        env_dict={
            "deployment_name": deployment_name,
            "user": component_config["user"],
        },
    )

    systemd.daemon_reload()
    systemd.service(
        systemd_unit,
        running=True,
        restarted=repo_output.changed,
        enabled=True,
        name=f"deploy d1 systemd unit ({systemd_unit})",
    )
