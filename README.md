# infra public

public copy of our deployment scripts for study and possible enhancement

the scripts here are not ready for public consumption. some assembly required.

## using

```
git clone https://gitlab.com/lavatech/infra-public
cd infra-public
# [...after assembling what you want...]
pyinfra inventory/whateveryouwant.py deploy_world.py
```

## contributing
