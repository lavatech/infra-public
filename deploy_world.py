import importlib
from pyinfra import host, local
from pyinfra.operations import server
from pyinfra.facts.server import LinuxName

# run this on all machines
if LinuxName == "Alpine":
    # make sure template operations work smoothly
    # because they run under smtp
    apk.packages(
        name="add openssh sftp package for file operations",
        packages=[
            "openssh-sftp-server",
        ],
    )

    apk.packages(
        name="add nice utils for all alpine machines",
        packages=[
            "curl",
            "bash",
            "coreutils",
            "shadow",
        ],
    )

    # TODO configure repositories, maybe?

# TODO: ssh key installation for ALL machines

# this goes for specific component deployments

# we store everything before executing so that we can make sure
# syntax errors appear quicker (by importing everything first)
# (calling the function directly would make pyinfra fetch the
# host facts, which takes a while as we are dealing with the network.)
def extract_components_and_run():
    to_deploy = []

    for component in host.data.dict().get("components", []):
        component_type = component["type"]
        component_module = importlib.import_module(f"tasks.install_{component_type}")
        component_function = getattr(component_module, f"install_{component_type}")
        to_deploy.append((component_function, component))

    for package in host.data.dict().get("packages", []):
        package_module = importlib.import_module(f"tasks.install_{package}")
        deploy_function = getattr(package_module, f"install_{package}")
        to_deploy.append((deploy_function,))

    print(to_deploy)

    for deploy_tuple in to_deploy:
        if len(deploy_tuple) == 1:
            deploy_tuple[0]()
        elif len(deploy_tuple) == 2:
            func, arg = deploy_tuple
            func(arg)
        else:
            raise Exception(f"Invalid deploy tuple. {deploy_tuple!r}")


if any(group.endswith("_hosts") for group in host.groups):
    extract_components_and_run()
